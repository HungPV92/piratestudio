package com.hungpham.piratestudio;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.hungpham.piratestudio.injection.component.ApplicationComponent;
import com.hungpham.piratestudio.injection.component.DaggerApplicationComponent;
import com.hungpham.piratestudio.injection.module.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by HungPham on 4/19/2017.
 */

public class PirateStudioApplication extends Application {

    private static PirateStudioApplication instance;
    private ApplicationComponent mApplicationComponent;

    public static PirateStudioApplication getInstance () {
        return instance;
    }

    public static PirateStudioApplication get(Context context){
        return (PirateStudioApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static boolean hasNetwork () {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public synchronized ApplicationComponent getComponent(){
        if (mApplicationComponent == null){
            mApplicationComponent = DaggerApplicationComponent
                    .builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}
