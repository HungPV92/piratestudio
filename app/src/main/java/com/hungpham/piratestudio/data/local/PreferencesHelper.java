package com.hungpham.piratestudio.data.local;

import com.hungpham.piratestudio.data.prefs.UserPrefs;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hungpham on 4/11/17.
 */
@Singleton
public class PreferencesHelper {
    private final UserPrefs mUserPrefs;

    @Inject
    public PreferencesHelper(UserPrefs userPrefs){
        this.mUserPrefs = userPrefs;
    }

    public UserPrefs getUserPrefs(){
        return mUserPrefs;
    }
}
