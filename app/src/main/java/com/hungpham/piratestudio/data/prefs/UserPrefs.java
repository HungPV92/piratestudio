package com.hungpham.piratestudio.data.prefs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.hungpham.piratestudio.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hungpham on 4/11/17.
 */
@Singleton
public class UserPrefs {
    private final SharedPreferences mSharePreferences;
    private final SharedPreferences.Editor mEditor;

    @SuppressLint("CommitPrefEdits")
    @Inject
    public UserPrefs(@ApplicationContext Context context){
        mSharePreferences = context.getSharedPreferences(UserPrefConst.PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharePreferences.edit();
    }
}
