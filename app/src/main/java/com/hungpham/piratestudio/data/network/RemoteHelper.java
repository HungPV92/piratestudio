package com.hungpham.piratestudio.data.network;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hungpham.piratestudio.PirateStudioApplication;
import com.hungpham.piratestudio.model.network.GetCityResponse;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.realm.BuildConfig;
import io.realm.RealmObject;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by hungpham on 4/11/17.
 */

public interface RemoteHelper {
    static final String CACHE_CONTROL = "Cache-Control";
    String baseUrl = "http://api.mywork.com.vn/";

    class Creator{

        public static Retrofit provideRemoteClient() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create(provideGson()))
                    .client(provideOkHttpClient())
                    .build();

            return retrofit;
        }


        static Gson provideGson(){
            return new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }}).setLenient().create();
        }

        static OkHttpClient provideOkHttpClient(){
            return new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();

                            Request request = original.newBuilder()
                                    .header("Authorization","Bearer puS78mGDBp88ByxgEy0Uuox68sWW1b4k")
                                    .method(original.method(), original.body())
                                    .build();
                            return chain.proceed(request);
                        }
                    })
                    .readTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .addInterceptor(provideLoggingInterceptor())
                    .addInterceptor(provideOfflineCacheInterceptor())
                    .addNetworkInterceptor(provideCacheInterceptor())
                    .cache(provideCache())
                    .build();
        }

        static HttpLoggingInterceptor provideLoggingInterceptor(){
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                    HttpLoggingInterceptor.Level.NONE);
            return loggingInterceptor;
        }

        public static Interceptor provideCacheInterceptor () {
            return new Interceptor()
            {
                @Override
                public Response intercept (Chain chain) throws IOException
                {
                    Response response = chain.proceed( chain.request() );

                    // re-write response header to force use of cache
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxAge( 2, TimeUnit.MINUTES )
                            .build();

                    return response.newBuilder()
                            .header( CACHE_CONTROL, cacheControl.toString() )
                            .build();
                }
            };
        }

        public static Interceptor provideOfflineCacheInterceptor () {
            return new Interceptor() {
                @Override
                public Response intercept (Chain chain) throws IOException {
                    Request request = chain.request();

                    if ( !PirateStudioApplication.hasNetwork() ) {
                        CacheControl cacheControl = new CacheControl.Builder()
                                .maxStale( 7, TimeUnit.DAYS )
                                .build();

                        request = request.newBuilder()
                                .cacheControl( cacheControl )
                                .build();
                    }
                    return chain.proceed( request );
                }
            };
        }

        private static Cache provideCache () {
            Cache cache = null;
            try {
                cache = new Cache( new File( PirateStudioApplication.getInstance().getCacheDir(), "http-cache" ),
                        10 * 1024 * 1024 ); // 10 MB
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return cache;
        }
    }

    class NullOnEmptyConverterFactory extends Converter.Factory{
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {

                @Override
                public Object convert(ResponseBody value) throws IOException {
                    if (value.contentLength() == 0) return null;
                    return delegate.convert(value);
                }
            };
        }
    }

    @GET("options")
    Observable<GetCityResponse> getCityOption(
            @Query("type") String type);
}
