package com.hungpham.piratestudio.data;

import android.app.Application;

import com.hungpham.piratestudio.data.local.PreferencesHelper;
import com.hungpham.piratestudio.data.local.RealmHelper;
import com.hungpham.piratestudio.data.network.RemoteHelper;
import com.hungpham.piratestudio.model.local.City;
import com.hungpham.piratestudio.model.network.BaseResponse;
import com.hungpham.piratestudio.model.network.GetCityResponse;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by HungPham on 4/19/2017.
 */
@Singleton
public class DataManager {
    private final RemoteHelper mRemoteHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final RealmHelper mRealmHelper;
    private final Application mContext;

    @Inject
    public DataManager(RemoteHelper mRemoteHelper, PreferencesHelper mPreferencesHelper, RealmHelper mRealmHelper, Application mContext) {
        this.mRemoteHelper = mRemoteHelper;
        this.mPreferencesHelper = mPreferencesHelper;
        this.mRealmHelper = mRealmHelper;
        this.mContext = mContext;
    }

    public Observable<List<City>> getCityOption(){
        return mRemoteHelper.getCityOption("locations")
                .filter(BaseResponse::getStatus)
                .map(GetCityResponse::getData);
    }
}
