package com.hungpham.piratestudio.injection.component;

import android.app.Application;
import android.content.Context;

import com.hungpham.piratestudio.data.DataManager;
import com.hungpham.piratestudio.data.network.RemoteHelper;
import com.hungpham.piratestudio.injection.ApplicationContext;
import com.hungpham.piratestudio.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import io.realm.Realm;
import retrofit2.Retrofit;

/**
 * Created by HungPham on 4/19/2017.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    Application application();
    @ApplicationContext
    Context context();
    DataManager dataManager();
    Retrofit retrofit();
    Realm realm();
}
