package com.hungpham.piratestudio.injection.component;

import com.hungpham.piratestudio.injection.PerActivity;
import com.hungpham.piratestudio.injection.module.ActivityModule;
import com.hungpham.piratestudio.injection.module.FragmentModule;

import dagger.Component;

/**
 * Created by HungPham on 4/19/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, FragmentModule.class})
public interface FragmentComponent extends ActivityComponent {
}
