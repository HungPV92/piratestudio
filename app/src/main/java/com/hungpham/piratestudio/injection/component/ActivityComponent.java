package com.hungpham.piratestudio.injection.component;

import com.hungpham.piratestudio.screens.home.MainActivity;
import com.hungpham.piratestudio.injection.PerActivity;
import com.hungpham.piratestudio.injection.module.ActivityModule;

import dagger.Component;

/**
 * Created by HungPham on 4/19/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
