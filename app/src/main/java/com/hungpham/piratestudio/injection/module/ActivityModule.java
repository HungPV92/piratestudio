package com.hungpham.piratestudio.injection.module;

import android.app.Activity;
import android.content.Context;

import com.hungpham.piratestudio.injection.ActivityContext;
import com.hungpham.piratestudio.injection.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by HungPham on 4/19/2017.
 */
@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    @PerActivity
    Activity provideActivity(){
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext(){
        return mActivity;
    }
}
