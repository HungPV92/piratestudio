package com.hungpham.piratestudio.injection.module;

import android.app.Application;
import android.content.Context;

import com.hungpham.piratestudio.data.local.RealmHelper;
import com.hungpham.piratestudio.data.network.RemoteHelper;
import com.hungpham.piratestudio.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import retrofit2.Retrofit;

/**
 * Created by HungPham on 4/19/2017.
 */
@Module
public class ApplicationModule {

    protected final Application mApplication;

    public ApplicationModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    Application provideApplication(){
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideApplicationContext(){
        return mApplication;
    }

    @Provides
    RemoteHelper provideRemoteHelper(Retrofit retrofit){
        return retrofit.create(RemoteHelper.class);
    }

    @Provides
    RealmHelper provideRealmHelper(){
        return new RealmHelper();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(){
        return RemoteHelper.Creator.provideRemoteClient();
    }

    @Provides
    @Singleton
    Realm provideRealm(){
        return Realm.getDefaultInstance();
    }
}
