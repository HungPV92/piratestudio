package com.hungpham.piratestudio.model.local;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HungPham on 4/19/2017.
 */

public class City {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
