package com.hungpham.piratestudio.model.network;

import com.google.gson.annotations.SerializedName;
import com.hungpham.piratestudio.model.local.City;

import java.util.List;

/**
 * Created by HungPham on 4/19/2017.
 */

public class GetCityResponse extends BaseResponse {

    @SerializedName("data")
    private List<City> data = null;

    public List<City> getData() {
        return data;
    }

    public void setData(List<City> data) {
        this.data = data;
    }
}
