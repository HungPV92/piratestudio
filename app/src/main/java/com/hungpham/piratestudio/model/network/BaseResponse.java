package com.hungpham.piratestudio.model.network;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HungPham on 4/19/2017.
 */

public class BaseResponse {
    @Nullable
    @SerializedName("messages")
    private List<String> messages = null;
    @Nullable
    @SerializedName("status")
    private String status;

    @Nullable
    public List<String> getMessages() {
        return messages;
    }

    @Nullable
    public String resultMessages() {
        String msg = "";
        assert messages != null;
        for(int i = 0; i < messages.size(); i++){
            msg = i < messages.size() - 1 ?
                    msg + messages.get(i) + "\n" :
                    msg + messages.get(i);
        }
        return msg;
    }

    public void setMessages(@Nullable List<String> messages) {
        this.messages = messages;
    }

    @Nullable
    public boolean getStatus() {
        return status.equals("success");
    }

    public void setStatus(@Nullable String status) {
        this.status = status;
    }

    public boolean isSuccessful() {
        assert status != null;
        return status.equals("success");
    }

    public boolean isActiveAccount() {
        assert status != null;
        return status.equals("inactive");
    }
}
