package com.hungpham.piratestudio.screens.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hungpham.piratestudio.R;
import com.hungpham.piratestudio.base.BaseActivity;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements IHomeView {

    @Inject
    HomePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        mPresenter.attachView(this);

        mPresenter.getCities();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    protected int getMainLayout() {
        return R.layout.activity_main;
    }
}
