package com.hungpham.piratestudio.screens.home;

import android.util.Log;

import com.hungpham.piratestudio.base.BasePresenter;
import com.hungpham.piratestudio.data.DataManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by HungPham on 4/19/2017.
 */

public class HomePresenter extends BasePresenter<IHomeView> {

    private final DataManager mDataManager;

    @Inject
    public HomePresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }

    public void getCities(){
        checkViewAttached();
        getDisposable().add(mDataManager.getCityOption()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cities -> {
                    Log.e("","");
                }));
    }
}
