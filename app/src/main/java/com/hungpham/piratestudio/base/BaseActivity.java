package com.hungpham.piratestudio.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hungpham.piratestudio.PirateStudioApplication;
import com.hungpham.piratestudio.injection.component.ActivityComponent;
import com.hungpham.piratestudio.injection.component.DaggerActivityComponent;

import butterknife.ButterKnife;

/**
 * Created by HungPham on 4/19/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getMainLayout());
        ButterKnife.bind(this);
        ButterKnife.setDebug(true);
    }

    protected abstract int getMainLayout();

    public ActivityComponent getActivityComponent(){
        if (mActivityComponent == null){
            mActivityComponent = DaggerActivityComponent
                    .builder()
                    .applicationComponent(PirateStudioApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }
}
