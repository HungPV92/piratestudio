package com.hungpham.piratestudio.base;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by HungPham on 4/19/2017.
 */

public class BasePresenter<T extends IBaseView> implements IBasePresenter<T> {

    private T mView;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void attachView(T view) {
        if (mView == null)
            mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        disposable.clear();
    }

    public boolean isViewAttached() {
        return mView != null;
    }

    public T getView(){return mView;}

    public void checkViewAttached() {
        if (!isViewAttached()) {
            throw new MvpViewNotAttachedException();
        }
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    public CompositeDisposable getDisposable(){
        return disposable;
    }
}
